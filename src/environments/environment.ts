// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  title: 'default',
  firebaseConfig: {
    apiKey: 'AIzaSyAgsaiWEp44LWyTXzyLQ6IRlfP1S2DTX1A',
    authDomain: 'devcicd-cecc5.firebaseapp.com',
    projectId: 'devcicd-cecc5',
    storageBucket: 'devcicd-cecc5.appspot.com',
    messagingSenderId: '1035952257922',
    appId: '1:1035952257922:web:2d7a43a8673ce6e037ad9b',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
