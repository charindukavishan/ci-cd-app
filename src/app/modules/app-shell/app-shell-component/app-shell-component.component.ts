import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-shell-component',
  templateUrl: './app-shell-component.component.html',
  styleUrls: ['./app-shell-component.component.scss']
})
export class AppShellComponent implements OnInit {
  list = [1, 3, 4];
  isChecked = true;
  constructor() { }

  ngOnInit(): void {
  }
  onChange($event: any): void {
    // console.log($event);
  }
}
