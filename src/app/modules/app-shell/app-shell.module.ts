import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppShellRoutingModule } from './app-shell-routing.module';
import { AppShellComponent } from './app-shell-component/app-shell-component.component';
import { MdComponentsModule } from 'src/app/imports/md-components.module';


@NgModule({
  declarations: [AppShellComponent],
  imports: [
    CommonModule,
    MdComponentsModule,
    AppShellRoutingModule
  ]
})
export class AppShellModule { }
