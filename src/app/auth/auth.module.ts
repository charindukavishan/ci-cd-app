import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCheckboxModule} from '@angular/material/checkbox';


import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth/auth.component';


@NgModule({
  declarations: [AuthComponent],
  imports: [
    CommonModule,
    AuthRoutingModule,
    MatCheckboxModule
  ]
})
export class AuthModule { }
